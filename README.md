# Bootstrap new RouterOS device

Takes a Mikrotik RouterOS device, **formats it**, installs new/updated RouterOS version
and installs just enough configuration ready to be handed off to Ansible for
configuration management.

## Configuration

After this you will have a RouterOS device with the following minimalist configuration:

* one Ethernet interface (ether1) for management
* static IP
* initial password for `admin` user
* all services disabled except ssh
* no bridge
* no wan port
* all other ethernet ports disabled
* no DHCP server or client

## Preparation

You'll need a Linux host, with a dedicated ethernet port for directly connecting Mikrotik devices.  
Configure the dedicated ethernet port to have a static IP, not part of your
existing (wifi) network and always be up even when a cable/carrier is not present.

```
# /etc/systemd/network/50-eno2.network
[Match]
Name=eno2

[Network]
Address=192.168.1.10/24
ConfigureWithoutCarrier=yes
IgnoreCarrierLoss=yes
KeepConfiguration=yes
```

[Download](https://mikrotik.com/download) Mikrotik's Netinstall (CLI Linux) to [bin/](./bin/) and the latest RouterOS package for your CPU type to [downloads/](./downloads/).  
Read about [Netinstall](https://help.mikrotik.com/docs/display/ROS/Netinstall) paying attention to the Linux instructions.

## Format Device

* at the top of `scripts/initial.scr` update:
  * admin password
  * IP address
* start Netinstall
  * `./bin/netinstall-cli -r -s <scriptname> -a <tmp ip of device> <routeros package>`
  * `./bin/netinstall-cli -r -s scripts/initial.scr -a 192.168.1.253 downloads/routeros-7.3.1-mipsbe.npk`
* Connect ethernet cable into **ethernet port 1** of the Mikrotik device
* reboot Mikrotik device whilst holding reset button for >10 seconds
* release button once you see the device shows up in the Netinstall logs

After a few minutes the Netinstall logs should indicate that the device is now
ready and will reboot.

After rebooting, you should be able to login with ssh as the admin user.

The Mikrotik device now has just enough configuration ready to let Ansible
configure it.

<table>
  <tr>
    <th>Author</th><td>Mick Pollard (aussielunix)</td>
  </tr>
  <tr>
    <th>Copyright</th><td>Copyright (c) 2022 by Mick Pollard</td>
  </tr>
  <tr>
    <th>License</th><td>Distributed under the MIT License, see <a href="LICENSE">LICENSE</a></td>
  </tr>
  <tr>
    <th>twitter </th><td>[@aussielunix](https://twitter.com/aussielunix)</td>
  </tr>
</table>
